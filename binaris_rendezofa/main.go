package main

import "fmt"

type FaElem struct {
	Bal   *FaElem
	Ertek int
	Jobb  *FaElem
}

func main() {
	faElem1 := FaElem{Bal: nil, Ertek: 5, Jobb: nil}
	faElem2 := FaElem{Bal: nil, Ertek: 4, Jobb: &faElem1}
	faElem3 := FaElem{Bal: &faElem2, Ertek: 9, Jobb: nil}
	faElem4 := FaElem{Bal: nil, Ertek: 19, Jobb: nil}
	faElem5 := FaElem{Bal: nil, Ertek: 33, Jobb: nil}
	faElem6 := FaElem{Bal: &faElem4, Ertek: 25, Jobb: &faElem5}
	faElem7 := FaElem{Bal: &faElem3, Ertek: 13, Jobb: &faElem6} // ez a gyökér

	fmt.Println("Inorder:")
	Inorder(&faElem7)
	fmt.Printf("\nPreorder:\n")
	Preorder(&faElem7)
	fmt.Printf("\nPostorder:\n")
	Postorder(&faElem7)
	fmt.Printf("\nKeres:\n")
	keresett := &faElem3
	fmt.Printf("Keresett memória cím:\t %v\n", keresett)
	fmt.Printf("Talált memória cím:\t %v\n", Keres(keresett.Ertek, &faElem7))
}

func Inorder(gyoker *FaElem) {
	if gyoker != nil {
		Inorder(gyoker.Bal)
		fmt.Println(gyoker.Ertek)
		Inorder(gyoker.Jobb)
	}
}

func Preorder(gyoker *FaElem) {
	if gyoker != nil {
		fmt.Println(gyoker.Ertek)
		Preorder(gyoker.Bal)
		Preorder(gyoker.Jobb)
	}
}

func Postorder(gyoker *FaElem) {
	if gyoker != nil {
		Postorder(gyoker.Bal)
		Postorder(gyoker.Jobb)
		fmt.Println(gyoker.Ertek)
	}
}

func Keres(e int, gyoker *FaElem) *FaElem {
	var keres *FaElem
	if gyoker == nil {
		keres = nil
	} else if e < gyoker.Ertek {
		keres = Keres(e, gyoker.Bal)
	} else if e > gyoker.Ertek {
		keres = Keres(e, gyoker.Jobb)
	} else {
		keres = gyoker
	}

	return keres
}
